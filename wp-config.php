<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keysa
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'titbdemo_kspa1' );

/** MySQL database username */
define( 'DB_USER', 'titbdemo_kspa1' );

/** MySQL database password */
define( 'DB_PASSWORD', 'mRruM*K}MVv(' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'U,!k%:p^9zE6MI]Wduh#-dFrNb0O2p($IQ=3Rld00*0A7D>Nl`i5qX/Nb;x7[)8P' );
define( 'SECURE_AUTH_KEY',  'eI)%t5mXqRGnJ}<Sa:+S{Gh ,3^?L:X6@HYbE!j_7ER=<.fkH^COUIS-Fe{jq&0^' );
define( 'LOGGED_IN_KEY',    '0X%vWac6.j`T@w`5|~u9%a>#dp~M*!7W:;E?77ACFP8;{~^x}q}b=9x?mo7w2a/,' );
define( 'NONCE_KEY',        'Gm6y@rDN:~B0Fn78%iu,]_>N1h&`D;dF]}h61b$gr%?S,O6Ru):UXsmsW>3wy,Sb' );
define( 'AUTH_SALT',        'Y!TI$6C}6]%I[T89kJ*cQY<*WJR[:jk6Ivha!r-y#B5}*9=|MKaBEc/9tA2E_RGb' );
define( 'SECURE_AUTH_SALT', ')mlXs180W>$JbFq6bTqf-BdXis8l+*I.R-=[<Q?e4LK%kaPSO+zG+X{il76:*{^t' );
define( 'LOGGED_IN_SALT',   '$|Mu!Eiw9?_(vpZcXHMtG~@i?jCc}%W^Vxf7vnsIm$w;33t=eT0#u[[^|,QBW,n~' );
define( 'NONCE_SALT',       'B+XO2(18 ZCkj#*b0.2%9-T~zTiOi#(TV4eEu5${cPT#Lm^Y$=yTXU}~`9gXV!-s' );

define( 'WP_MAX_MEMORY_LIMIT', '512M' );
define( 'WP_MEMORY_LIMIT', '512M' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
