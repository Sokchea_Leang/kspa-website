# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-12-15 15:00+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: bt_bb_raw_content.php:22
msgid "Raw Content"
msgstr ""

#: bt_bb_raw_content.php:22
msgid "Raw HTML/JS content"
msgstr ""

#: bt_bb_raw_content.php:24
msgid "Raw content"
msgstr ""

#: bt_bb_twitter.php:126 medicare.php:5135
msgid "Twitter"
msgstr ""

#: bt_bb_twitter.php:126
msgid "Twitter posts"
msgstr ""

#: bt_bb_twitter.php:128
msgid "Number of Tweets"
msgstr ""

#: bt_bb_twitter.php:129 medicare.php:3855
msgid "Type"
msgstr ""

#: bt_bb_twitter.php:131 medicare.php:3857 medicare.php:3983
msgid "Slider"
msgstr ""

#: bt_bb_twitter.php:132
msgid "Regular grid"
msgstr ""

#: bt_bb_twitter.php:134
msgid "Show avatar"
msgstr ""

#: bt_bb_twitter.php:136 medicare.php:3422 medicare.php:3460 medicare.php:3469
#: medicare.php:3484 medicare.php:3496 medicare.php:3502 medicare.php:3601
#: medicare.php:3606 medicare.php:3642 medicare.php:3694 medicare.php:3757
#: medicare.php:3807 medicare.php:3889 medicare.php:3995 medicare.php:4000
#: medicare.php:4022 medicare.php:4054 medicare.php:4063 medicare.php:4072
#: medicare.php:4123 medicare.php:4129 medicare.php:4180 medicare.php:4185
#: medicare.php:4190 medicare.php:4195 medicare.php:4200 medicare.php:4230
#: medicare.php:4257 medicare.php:4267 medicare.php:4272
msgid "No"
msgstr ""

#: bt_bb_twitter.php:137 medicare.php:3423 medicare.php:3485 medicare.php:3497
#: medicare.php:3503 medicare.php:3602 medicare.php:3607 medicare.php:3643
#: medicare.php:3758 medicare.php:3996 medicare.php:4001 medicare.php:4023
#: medicare.php:4073 medicare.php:4124 medicare.php:4179 medicare.php:4184
#: medicare.php:4189 medicare.php:4194 medicare.php:4199 medicare.php:4231
#: medicare.php:4258 medicare.php:4268 medicare.php:4273
msgid "Yes"
msgstr ""

#: bt_bb_twitter.php:139
msgid "Number of slides to show"
msgstr ""

#: bt_bb_twitter.php:139
msgid "(1-6) e.g. 3"
msgstr ""

#: bt_bb_twitter.php:140
msgid "Gap"
msgstr ""

#: bt_bb_twitter.php:142
msgid "No gap"
msgstr ""

#: bt_bb_twitter.php:143 medicare.php:3686 medicare.php:3797 medicare.php:3917
#: medicare.php:3988 medicare.php:4108
msgid "Small"
msgstr ""

#: bt_bb_twitter.php:144 medicare.php:3924 medicare.php:4144
msgid "Normal"
msgstr ""

#: bt_bb_twitter.php:145 medicare.php:3688 medicare.php:3801 medicare.php:3990
#: medicare.php:4112
msgid "Large"
msgstr ""

#: bt_bb_twitter.php:148
msgid "Autoplay interval (ms)"
msgstr ""

#: bt_bb_twitter.php:148
msgid "e.g. 2000"
msgstr ""

#: bt_bb_twitter.php:149
msgid "Username (or #hashtag)"
msgstr ""

#: bt_bb_twitter.php:150
msgid "Cache (minutes)"
msgstr ""

#: bt_bb_twitter.php:152
msgid "Consumer Key"
msgstr ""

#: bt_bb_twitter.php:152 bt_bb_twitter.php:153 bt_bb_twitter.php:154
#: bt_bb_twitter.php:155
msgid "API"
msgstr ""

#: bt_bb_twitter.php:153
msgid "Consumer Secret"
msgstr ""

#: bt_bb_twitter.php:154
msgid "Access Token"
msgstr ""

#: bt_bb_twitter.php:155
msgid "Access Token Secret"
msgstr ""

#: medicare.php:26
msgid "Custom JS (Top)"
msgstr ""

#: medicare.php:42
msgid "Custom JS (Bottom)"
msgstr ""

#: medicare.php:59 medicare.php:136 medicare.php:213
msgid "Share on Facebook"
msgstr ""

#: medicare.php:74 medicare.php:151 medicare.php:228
msgid "Share on Twitter"
msgstr ""

#: medicare.php:89 medicare.php:166 medicare.php:243
msgid "Share on Google Plus"
msgstr ""

#: medicare.php:104 medicare.php:181 medicare.php:258
msgid "Share on LinkedIn"
msgstr ""

#: medicare.php:119 medicare.php:196 medicare.php:273
msgid "Share on VK"
msgstr ""

#: medicare.php:2969
msgid "Category filter:"
msgstr ""

#: medicare.php:2970
msgid "All"
msgstr ""

#: medicare.php:2980
msgid "No more posts"
msgstr ""

#: medicare.php:3410 medicare.php:3412 medicare.php:3726 medicare.php:3875
#: medicare.php:4042
msgid "Image"
msgstr ""

#: medicare.php:3410
msgid "Single image"
msgstr ""

#: medicare.php:3413
msgid "Caption Text"
msgstr ""

#: medicare.php:3414
msgid "Size (e.g. thumbnail, medium, large, full)"
msgstr ""

#: medicare.php:3415
msgid "Shape"
msgstr ""

#: medicare.php:3417
msgid "Square"
msgstr ""

#: medicare.php:3418
msgid "Circle"
msgstr ""

#: medicare.php:3420 medicare.php:3500
msgid "Lazy load this image"
msgstr ""

#: medicare.php:3425 medicare.php:3803 medicare.php:3876 medicare.php:3886
#: medicare.php:4126
msgid "URL"
msgstr ""

#: medicare.php:3426
msgid "Target"
msgstr ""

#: medicare.php:3428 medicare.php:3890 medicare.php:4130
msgid "Self"
msgstr ""

#: medicare.php:3429 medicare.php:3891 medicare.php:4131
msgid "Blank"
msgstr ""

#: medicare.php:3431
msgid "Hover type"
msgstr ""

#: medicare.php:3433 medicare.php:3582 medicare.php:3629 medicare.php:3785
#: medicare.php:3791 medicare.php:4017 medicare.php:4096 medicare.php:4116
msgid "Default"
msgstr ""

#: medicare.php:3434
msgid "Simple"
msgstr ""

#: medicare.php:3436 medicare.php:3544 medicare.php:3663 medicare.php:3705
#: medicare.php:3744 medicare.php:3760 medicare.php:3775 medicare.php:3811
#: medicare.php:3838 medicare.php:3860 medicare.php:3877 medicare.php:3927
#: medicare.php:3945 medicare.php:3968 medicare.php:4025 medicare.php:4043
#: medicare.php:4075 medicare.php:4148 medicare.php:4202 medicare.php:4275
#: medicare.php:4319 medicare.php:4342 medicare.php:4361 medicare.php:4379
msgid "Publish date"
msgstr ""

#: medicare.php:3436 medicare.php:3437 medicare.php:3544 medicare.php:3545
#: medicare.php:3663 medicare.php:3664 medicare.php:3705 medicare.php:3706
#: medicare.php:3744 medicare.php:3745 medicare.php:3760 medicare.php:3761
#: medicare.php:3775 medicare.php:3776 medicare.php:3811 medicare.php:3812
#: medicare.php:3838 medicare.php:3839 medicare.php:3860 medicare.php:3861
#: medicare.php:3877 medicare.php:3878 medicare.php:3927 medicare.php:3928
#: medicare.php:3945 medicare.php:3946 medicare.php:3968 medicare.php:3969
#: medicare.php:4025 medicare.php:4026 medicare.php:4043 medicare.php:4044
#: medicare.php:4075 medicare.php:4076 medicare.php:4148 medicare.php:4149
#: medicare.php:4202 medicare.php:4203 medicare.php:4275 medicare.php:4276
#: medicare.php:4319 medicare.php:4320 medicare.php:4342 medicare.php:4343
#: medicare.php:4361 medicare.php:4362 medicare.php:4379 medicare.php:4380
msgid "Please, fill both the date and time"
msgstr ""

#: medicare.php:3436 medicare.php:3437 medicare.php:3438 medicare.php:3439
#: medicare.php:3513 medicare.php:3514 medicare.php:3515 medicare.php:3544
#: medicare.php:3545 medicare.php:3613 medicare.php:3614 medicare.php:3647
#: medicare.php:3648 medicare.php:3663 medicare.php:3664 medicare.php:3665
#: medicare.php:3666 medicare.php:3705 medicare.php:3706 medicare.php:3707
#: medicare.php:3708 medicare.php:3744 medicare.php:3745 medicare.php:3760
#: medicare.php:3761 medicare.php:3775 medicare.php:3776 medicare.php:3811
#: medicare.php:3812 medicare.php:3813 medicare.php:3814 medicare.php:3838
#: medicare.php:3839 medicare.php:3840 medicare.php:3841 medicare.php:3860
#: medicare.php:3861 medicare.php:3877 medicare.php:3878 medicare.php:3927
#: medicare.php:3928 medicare.php:3929 medicare.php:3930 medicare.php:3945
#: medicare.php:3946 medicare.php:3968 medicare.php:3969 medicare.php:3970
#: medicare.php:3971 medicare.php:4025 medicare.php:4026 medicare.php:4027
#: medicare.php:4028 medicare.php:4043 medicare.php:4044 medicare.php:4045
#: medicare.php:4046 medicare.php:4075 medicare.php:4076 medicare.php:4077
#: medicare.php:4078 medicare.php:4135 medicare.php:4136 medicare.php:4148
#: medicare.php:4149 medicare.php:4150 medicare.php:4151 medicare.php:4202
#: medicare.php:4203 medicare.php:4204 medicare.php:4205 medicare.php:4275
#: medicare.php:4276 medicare.php:4277 medicare.php:4278 medicare.php:4319
#: medicare.php:4320 medicare.php:4321 medicare.php:4322 medicare.php:4342
#: medicare.php:4343 medicare.php:4361 medicare.php:4362 medicare.php:4363
#: medicare.php:4364 medicare.php:4379 medicare.php:4380 medicare.php:4381
#: medicare.php:4382
msgid "Custom"
msgstr ""

#: medicare.php:3437 medicare.php:3545 medicare.php:3664 medicare.php:3706
#: medicare.php:3745 medicare.php:3761 medicare.php:3776 medicare.php:3812
#: medicare.php:3839 medicare.php:3861 medicare.php:3878 medicare.php:3928
#: medicare.php:3946 medicare.php:3969 medicare.php:4026 medicare.php:4044
#: medicare.php:4076 medicare.php:4149 medicare.php:4203 medicare.php:4276
#: medicare.php:4320 medicare.php:4343 medicare.php:4362 medicare.php:4380
msgid "Expiry date"
msgstr ""

#: medicare.php:3438 medicare.php:3514 medicare.php:3529 medicare.php:3546
#: medicare.php:3613 medicare.php:3647 medicare.php:3665 medicare.php:3707
#: medicare.php:3813 medicare.php:3840 medicare.php:3929 medicare.php:3970
#: medicare.php:4027 medicare.php:4045 medicare.php:4077 medicare.php:4135
#: medicare.php:4150 medicare.php:4204 medicare.php:4277 medicare.php:4321
#: medicare.php:4344 medicare.php:4363 medicare.php:4381
msgid "Extra Class Name(s)"
msgstr ""

#: medicare.php:3439 medicare.php:3515 medicare.php:3530 medicare.php:3547
#: medicare.php:3614 medicare.php:3648 medicare.php:3666 medicare.php:3708
#: medicare.php:3814 medicare.php:3841 medicare.php:3930 medicare.php:3971
#: medicare.php:4028 medicare.php:4046 medicare.php:4078 medicare.php:4136
#: medicare.php:4151 medicare.php:4205 medicare.php:4278 medicare.php:4322
#: medicare.php:4345 medicare.php:4364 medicare.php:4382
msgid "Inline Style"
msgstr ""

#: medicare.php:3440 medicare.php:3516 medicare.php:3531 medicare.php:3548
#: medicare.php:3649 medicare.php:3667 medicare.php:3709 medicare.php:3736
#: medicare.php:3762 medicare.php:3815 medicare.php:3842 medicare.php:3862
#: medicare.php:3931 medicare.php:3947 medicare.php:3972 medicare.php:4029
#: medicare.php:4079 medicare.php:4152 medicare.php:4206 medicare.php:4279
#: medicare.php:4323 medicare.php:4346 medicare.php:4365 medicare.php:4383
msgid "Hide element on screens"
msgstr ""

#: medicare.php:3440 medicare.php:3516 medicare.php:3531 medicare.php:3548
#: medicare.php:3649 medicare.php:3667 medicare.php:3709 medicare.php:3736
#: medicare.php:3762 medicare.php:3815 medicare.php:3842 medicare.php:3862
#: medicare.php:3931 medicare.php:3947 medicare.php:3972 medicare.php:4029
#: medicare.php:4079 medicare.php:4152 medicare.php:4206 medicare.php:4279
#: medicare.php:4323 medicare.php:4346 medicare.php:4365 medicare.php:4383
msgid "Responsive"
msgstr ""

#: medicare.php:3442 medicare.php:3518 medicare.php:3533 medicare.php:3550
#: medicare.php:3651 medicare.php:3669 medicare.php:3711 medicare.php:3738
#: medicare.php:3764 medicare.php:3817 medicare.php:3844 medicare.php:3864
#: medicare.php:3933 medicare.php:3949 medicare.php:3974 medicare.php:4031
#: medicare.php:4081 medicare.php:4154 medicare.php:4208 medicare.php:4281
#: medicare.php:4325 medicare.php:4348 medicare.php:4367 medicare.php:4385
msgid "<480px"
msgstr ""

#: medicare.php:3443 medicare.php:3519 medicare.php:3534 medicare.php:3551
#: medicare.php:3652 medicare.php:3670 medicare.php:3712 medicare.php:3739
#: medicare.php:3765 medicare.php:3818 medicare.php:3845 medicare.php:3865
#: medicare.php:3934 medicare.php:3950 medicare.php:3975 medicare.php:4032
#: medicare.php:4082 medicare.php:4155 medicare.php:4209 medicare.php:4282
#: medicare.php:4326 medicare.php:4349 medicare.php:4368 medicare.php:4386
msgid "480-767px"
msgstr ""

#: medicare.php:3444 medicare.php:3520 medicare.php:3535 medicare.php:3552
#: medicare.php:3653 medicare.php:3671 medicare.php:3713 medicare.php:3740
#: medicare.php:3766 medicare.php:3819 medicare.php:3846 medicare.php:3866
#: medicare.php:3935 medicare.php:3951 medicare.php:3976 medicare.php:4033
#: medicare.php:4083 medicare.php:4156 medicare.php:4210 medicare.php:4283
#: medicare.php:4327 medicare.php:4350 medicare.php:4369 medicare.php:4387
msgid "768-991px"
msgstr ""

#: medicare.php:3445 medicare.php:3521 medicare.php:3536 medicare.php:3553
#: medicare.php:3654 medicare.php:3672 medicare.php:3714 medicare.php:3741
#: medicare.php:3767 medicare.php:3820 medicare.php:3847 medicare.php:3867
#: medicare.php:3936 medicare.php:3952 medicare.php:3977 medicare.php:4034
#: medicare.php:4084 medicare.php:4157 medicare.php:4211 medicare.php:4284
#: medicare.php:4328 medicare.php:4351 medicare.php:4370 medicare.php:4388
msgid "992-1200px"
msgstr ""

#: medicare.php:3446 medicare.php:3522 medicare.php:3537 medicare.php:3554
#: medicare.php:3655 medicare.php:3673 medicare.php:3715 medicare.php:3742
#: medicare.php:3768 medicare.php:3821 medicare.php:3848 medicare.php:3868
#: medicare.php:3937 medicare.php:3953 medicare.php:3978 medicare.php:4035
#: medicare.php:4085 medicare.php:4158 medicare.php:4212 medicare.php:4285
#: medicare.php:4329 medicare.php:4352 medicare.php:4371 medicare.php:4389
msgid ">=1200px"
msgstr ""

#: medicare.php:3451
msgid "Section"
msgstr ""

#: medicare.php:3451
msgid "Basic root element"
msgstr ""

#: medicare.php:3453
msgid "Layout"
msgstr ""

#: medicare.php:3455
msgid "Boxed"
msgstr ""

#: medicare.php:3456
msgid "Wide"
msgstr ""

#: medicare.php:3458 medicare.php:4052
msgid "Top spaced"
msgstr ""

#: medicare.php:3461 medicare.php:3470 medicare.php:4055 medicare.php:4064
msgid "Extra-Small-Spaced"
msgstr ""

#: medicare.php:3462 medicare.php:3471 medicare.php:4056 medicare.php:4065
msgid "Small-Spaced"
msgstr ""

#: medicare.php:3463 medicare.php:3472 medicare.php:4057 medicare.php:4066
msgid "Semi-Spaced"
msgstr ""

#: medicare.php:3464 medicare.php:3473 medicare.php:4058 medicare.php:4067
msgid "Spaced"
msgstr ""

#: medicare.php:3465 medicare.php:3474 medicare.php:4059 medicare.php:4068
msgid "Extra-Spaced"
msgstr ""

#: medicare.php:3467 medicare.php:4061
msgid "Bottom spaced"
msgstr ""

#: medicare.php:3476
msgid "Skin"
msgstr ""

#: medicare.php:3476 medicare.php:3499 medicare.php:3500 medicare.php:3505
#: medicare.php:3506 medicare.php:3507 medicare.php:3508 medicare.php:3509
msgid "Background"
msgstr ""

#: medicare.php:3478 medicare.php:3489 medicare.php:3569 medicare.php:3635
msgid "Inherit"
msgstr ""

#: medicare.php:3479 medicare.php:3909
msgid "Dark"
msgstr ""

#: medicare.php:3480 medicare.php:3912
msgid "Light"
msgstr ""

#: medicare.php:3482
msgid "Full Screen"
msgstr ""

#: medicare.php:3487 medicare.php:3567 medicare.php:3633
msgid "Vertical Align"
msgstr ""

#: medicare.php:3490 medicare.php:3570 medicare.php:3636 medicare.php:3695
#: medicare.php:3893 medicare.php:4007 medicare.php:4133
msgid "Top"
msgstr ""

#: medicare.php:3491 medicare.php:3571 medicare.php:3637 medicare.php:4005
msgid "Middle"
msgstr ""

#: medicare.php:3492 medicare.php:3572 medicare.php:3638 medicare.php:3696
#: medicare.php:3808 medicare.php:4006 medicare.php:4011
msgid "Bottom"
msgstr ""

#: medicare.php:3494
msgid "Divider"
msgstr ""

#: medicare.php:3499
msgid "Background Image"
msgstr ""

#: medicare.php:3505 medicare.php:3609 medicare.php:3645
msgid "Background color"
msgstr ""

#: medicare.php:3506
msgid "YouTube Background Video"
msgstr ""

#: medicare.php:3507
msgid "Video Settings (e.g. startAt:20, mute:true, stopMovieOnBlur:false)"
msgstr ""

#: medicare.php:3508
msgid "Parallax (e.g. -.7)"
msgstr ""

#: medicare.php:3509
msgid "Parallax Offset in px (e.g. -100)"
msgstr ""

#: medicare.php:3510
msgid "Animation (forward)"
msgstr ""

#: medicare.php:3510 medicare.php:3511 medicare.php:3512
msgid "Animations"
msgstr ""

#: medicare.php:3511
msgid "Animation (backward)"
msgstr ""

#: medicare.php:3512
msgid "Impress Animation Settings"
msgstr ""

#: medicare.php:3513
msgid "Custom Id Attribute"
msgstr ""

#: medicare.php:3527
msgid "Row"
msgstr ""

#: medicare.php:3527
msgid "Row element"
msgstr ""

#: medicare.php:3542
msgid "Inner Row"
msgstr ""

#: medicare.php:3542
msgid "Inner Row element"
msgstr ""

#: medicare.php:3559
msgid "Column"
msgstr ""

#: medicare.php:3559
msgid "Column element"
msgstr ""

#: medicare.php:3561 medicare.php:3621
msgid "Align"
msgstr ""

#: medicare.php:3563 medicare.php:3577 medicare.php:3623 medicare.php:3905
msgid "Left"
msgstr ""

#: medicare.php:3564 medicare.php:3578 medicare.php:3624 medicare.php:3903
msgid "Right"
msgstr ""

#: medicare.php:3565 medicare.php:3625
msgid "Center"
msgstr ""

#: medicare.php:3574 medicare.php:4070
msgid "Border"
msgstr ""

#: medicare.php:3576
msgid "No Border"
msgstr ""

#: medicare.php:3580 medicare.php:3627
msgid "Padding"
msgstr ""

#: medicare.php:3583 medicare.php:3630
msgid "No padding"
msgstr ""

#: medicare.php:3584 medicare.php:3631
msgid "Double padding"
msgstr ""

#: medicare.php:3586 medicare.php:4015
msgid "Animation"
msgstr ""

#: medicare.php:3588
msgid "No Animation"
msgstr ""

#: medicare.php:3589
msgid "Fade In"
msgstr ""

#: medicare.php:3590
msgid "Move Up"
msgstr ""

#: medicare.php:3591
msgid "Move Left"
msgstr ""

#: medicare.php:3592
msgid "Move Right"
msgstr ""

#: medicare.php:3593
msgid "Move Down"
msgstr ""

#: medicare.php:3594
msgid "Fade In / Move Up"
msgstr ""

#: medicare.php:3595
msgid "Fade In / Move Left"
msgstr ""

#: medicare.php:3596
msgid "Fade In / Move Right"
msgstr ""

#: medicare.php:3597
msgid "Fade In / Move Down"
msgstr ""

#: medicare.php:3599
msgid "Text indent"
msgstr ""

#: medicare.php:3604 medicare.php:3640
msgid "Highlight"
msgstr ""

#: medicare.php:3610 medicare.php:3646
msgid "Opacity (0 - 1, e.g. 0.4)"
msgstr ""

#: medicare.php:3611
msgid "Inner Background color"
msgstr ""

#: medicare.php:3612
msgid "Background image"
msgstr ""

#: medicare.php:3619
msgid "Inner Column"
msgstr ""

#: medicare.php:3619
msgid "Inner Column element"
msgstr ""

#: medicare.php:3660
msgid "Custom Menu"
msgstr ""

#: medicare.php:3662
msgid "Menu Name"
msgstr ""

#: medicare.php:3678 medicare.php:3727 medicare.php:3810 medicare.php:3884
#: medicare.php:3960
msgid "Text"
msgstr ""

#: medicare.php:3678
msgid "Text element"
msgstr ""

#: medicare.php:3680
msgid "Header"
msgstr ""

#: medicare.php:3680
msgid "Header element"
msgstr ""

#: medicare.php:3682
msgid "Superheadline"
msgstr ""

#: medicare.php:3683 medicare.php:3725 medicare.php:3750 medicare.php:3774
#: medicare.php:3804
msgid "Headline"
msgstr ""

#: medicare.php:3684
msgid "Headline Size"
msgstr ""

#: medicare.php:3687 medicare.php:3799 medicare.php:3918 medicare.php:3989
#: medicare.php:4110
msgid "Medium"
msgstr ""

#: medicare.php:3689
msgid "Extra large"
msgstr ""

#: medicare.php:3690
msgid "Huge"
msgstr ""

#: medicare.php:3692 medicare.php:3805
msgid "Dash"
msgstr ""

#: medicare.php:3698
msgid "Dash Style"
msgstr ""

#: medicare.php:3700
msgid "Normal color"
msgstr ""

#: medicare.php:3701
msgid "Accent color"
msgstr ""

#: medicare.php:3702
msgid "Alternate color"
msgstr ""

#: medicare.php:3704
msgid "Subheadline"
msgstr ""

#: medicare.php:3720
msgid "Testimonials"
msgstr ""

#: medicare.php:3720
msgid "Testimonials container"
msgstr ""

#: medicare.php:3723
msgid "Testimonial Item"
msgstr ""

#: medicare.php:3723
msgid "Single testimonial item"
msgstr ""

#: medicare.php:3728
msgid "Name"
msgstr ""

#: medicare.php:3729
msgid "Job"
msgstr ""

#: medicare.php:3734
msgid "Tabs"
msgstr ""

#: medicare.php:3734
msgid "Tabs container"
msgstr ""

#: medicare.php:3749
msgid "Tab Item"
msgstr ""

#: medicare.php:3749
msgid "Tabs items"
msgstr ""

#: medicare.php:3754
msgid "Accordion"
msgstr ""

#: medicare.php:3754
msgid "Accordion container"
msgstr ""

#: medicare.php:3755
msgid "Open first item initially"
msgstr ""

#: medicare.php:3773
msgid "Accordion Item"
msgstr ""

#: medicare.php:3773
msgid "Single accordion element"
msgstr ""

#: medicare.php:3780
msgid "Service"
msgstr ""

#: medicare.php:3780
msgid "Service element"
msgstr ""

#: medicare.php:3782 medicare.php:3832 medicare.php:3885 medicare.php:4090
#: medicare.php:4092
msgid "Icon"
msgstr ""

#: medicare.php:3783 medicare.php:4094
msgid "Icon Type"
msgstr ""

#: medicare.php:3786 medicare.php:3898 medicare.php:3965 medicare.php:4097
msgid "Filled"
msgstr ""

#: medicare.php:3787 medicare.php:3897 medicare.php:4098
msgid "Outline"
msgstr ""

#: medicare.php:3789 medicare.php:4100
msgid "Icon Color"
msgstr ""

#: medicare.php:3792 medicare.php:3910
msgid "Accent"
msgstr ""

#: medicare.php:3793
msgid "Alter"
msgstr ""

#: medicare.php:3795 medicare.php:4106
msgid "Icon Size"
msgstr ""

#: medicare.php:3798 medicare.php:3916 medicare.php:4109
msgid "Extra Small"
msgstr ""

#: medicare.php:3800 medicare.php:3919 medicare.php:4111
msgid "Big"
msgstr ""

#: medicare.php:3826
msgid "Google Maps"
msgstr ""

#: medicare.php:3826
msgid "Google Maps with marker on specified coordinates"
msgstr ""

#: medicare.php:3828
msgid "API key"
msgstr ""

#: medicare.php:3829
msgid "Latitude"
msgstr ""

#: medicare.php:3830
msgid "Longitude"
msgstr ""

#: medicare.php:3831
msgid "Zoom (e.g. 14)"
msgstr ""

#: medicare.php:3833
msgid "Height (e.g. 250px)"
msgstr ""

#: medicare.php:3834
msgid "Map primary color"
msgstr ""

#: medicare.php:3835
msgid "Map secondary color"
msgstr ""

#: medicare.php:3836
msgid "Map water color"
msgstr ""

#: medicare.php:3837
msgid "Custom map style array"
msgstr ""

#: medicare.php:3853
msgid "Clients"
msgstr ""

#: medicare.php:3853
msgid "Client container"
msgstr ""

#: medicare.php:3858
msgid "Regular"
msgstr ""

#: medicare.php:3873
msgid "Client"
msgstr ""

#: medicare.php:3873
msgid "Individual client element"
msgstr ""

#: medicare.php:3882
msgid "Button"
msgstr ""

#: medicare.php:3882
msgid "Button with custom link"
msgstr ""

#: medicare.php:3887 medicare.php:4127
msgid "Target window"
msgstr ""

#: medicare.php:3892 medicare.php:4132
msgid "Parent"
msgstr ""

#: medicare.php:3895 medicare.php:3963
msgid "Style"
msgstr ""

#: medicare.php:3899
msgid "Borderless"
msgstr ""

#: medicare.php:3901
msgid "Icon Position"
msgstr ""

#: medicare.php:3904
msgid "Inline"
msgstr ""

#: medicare.php:3907 medicare.php:3962
msgid "Color"
msgstr ""

#: medicare.php:3911
msgid "Alternate"
msgstr ""

#: medicare.php:3914
msgid "Size"
msgstr ""

#: medicare.php:3922
msgid "Width"
msgstr ""

#: medicare.php:3925
msgid "Full"
msgstr ""

#: medicare.php:3942
msgid "Counter"
msgstr ""

#: medicare.php:3942
msgid "Animated counter"
msgstr ""

#: medicare.php:3944
msgid "Number"
msgstr ""

#: medicare.php:3958
msgid "Percentage bar"
msgstr ""

#: medicare.php:3958
msgid "Animated percentage bar"
msgstr ""

#: medicare.php:3961
msgid "Percentage"
msgstr ""

#: medicare.php:3966
msgid "Line"
msgstr ""

#: medicare.php:3983
msgid "Slider container"
msgstr ""

#: medicare.php:3985
msgid "Slider height"
msgstr ""

#: medicare.php:3987
msgid "Auto"
msgstr ""

#: medicare.php:3992
msgid "Auto Play Speed (e.g. 3000)"
msgstr ""

#: medicare.php:3993
msgid "Hide Arrows"
msgstr ""

#: medicare.php:3998
msgid "Hide Paging"
msgstr ""

#: medicare.php:4003
msgid "Arrows position"
msgstr ""

#: medicare.php:4009
msgid "Dots navigation"
msgstr ""

#: medicare.php:4012
msgid "Below"
msgstr ""

#: medicare.php:4013
msgid "Hide"
msgstr ""

#: medicare.php:4015
msgid "If fade is selected, number of slides to show will be 1"
msgstr ""

#: medicare.php:4018
msgid "Fade"
msgstr ""

#: medicare.php:4020
msgid "Simple Arrows"
msgstr ""

#: medicare.php:4040
msgid "Slider Item"
msgstr ""

#: medicare.php:4040
msgid "Individual slide element"
msgstr ""

#: medicare.php:4050
msgid "Separator"
msgstr ""

#: medicare.php:4050
msgid "Horizontal separator"
msgstr ""

#: medicare.php:4090
msgid "Single icon with link"
msgstr ""

#: medicare.php:4093 medicare.php:4336 medicare.php:4359
msgid "Title"
msgstr ""

#: medicare.php:4102
msgid "Default Color"
msgstr ""

#: medicare.php:4103
msgid "Accent Color"
msgstr ""

#: medicare.php:4104
msgid "Alternate Color"
msgstr ""

#: medicare.php:4114
msgid "Icon Animation"
msgstr ""

#: medicare.php:4117
msgid "Move from bottom"
msgstr ""

#: medicare.php:4118
msgid "Move from left"
msgstr ""

#: medicare.php:4119
msgid "Move from top"
msgstr ""

#: medicare.php:4121
msgid "Icon Outline"
msgstr ""

#: medicare.php:4140
msgid "Icon and images holder"
msgstr ""

#: medicare.php:4140
msgid "Icon container"
msgstr ""

#: medicare.php:4142
msgid "Position"
msgstr ""

#: medicare.php:4145
msgid "Over previous element (half height)"
msgstr ""

#: medicare.php:4146
msgid "Over previous element (full height)"
msgstr ""

#: medicare.php:4163
msgid "Latest Posts"
msgstr ""

#: medicare.php:4163
msgid "Recent posts"
msgstr ""

#: medicare.php:4165
msgid "Number of Items"
msgstr ""

#: medicare.php:4166 medicare.php:4227
msgid "Category Slug"
msgstr ""

#: medicare.php:4167
msgid "Format"
msgstr ""

#: medicare.php:4169
msgid "Horizontal"
msgstr ""

#: medicare.php:4170
msgid "Vertical"
msgstr ""

#: medicare.php:4172 medicare.php:4260
msgid "Post Type"
msgstr ""

#: medicare.php:4174 medicare.php:4262
msgid "Blog"
msgstr ""

#: medicare.php:4175 medicare.php:4263 medicare.php:5228
msgid "Portfolio"
msgstr ""

#: medicare.php:4177
msgid "Show Excerpt"
msgstr ""

#: medicare.php:4182
msgid "Show Date"
msgstr ""

#: medicare.php:4187
msgid "Show Author"
msgstr ""

#: medicare.php:4192
msgid "Show Comments"
msgstr ""

#: medicare.php:4197
msgid "Show Category"
msgstr ""

#: medicare.php:4217
msgid "Grid"
msgstr ""

#: medicare.php:4217
msgid "Grid with recent posts"
msgstr ""

#: medicare.php:4219
msgid "Number of items"
msgstr ""

#: medicare.php:4220 medicare.php:4311
msgid "Columns"
msgstr ""

#: medicare.php:4222 medicare.php:4243 medicare.php:4300 medicare.php:4313
msgid "3"
msgstr ""

#: medicare.php:4223 medicare.php:4244 medicare.php:4301 medicare.php:4314
msgid "4"
msgstr ""

#: medicare.php:4224 medicare.php:4245 medicare.php:4302 medicare.php:4315
msgid "5"
msgstr ""

#: medicare.php:4225 medicare.php:4246 medicare.php:4303 medicare.php:4316
msgid "6"
msgstr ""

#: medicare.php:4228
msgid "Category Filter"
msgstr ""

#: medicare.php:4233
msgid "Grid Type"
msgstr ""

#: medicare.php:4235
msgid "Classic"
msgstr ""

#: medicare.php:4236
msgid "Tiled"
msgstr ""

#: medicare.php:4238 medicare.php:4295
msgid "Grid Gap"
msgstr ""

#: medicare.php:4240 medicare.php:4297
msgid "0"
msgstr ""

#: medicare.php:4241 medicare.php:4298
msgid "1"
msgstr ""

#: medicare.php:4242 medicare.php:4299
msgid "2"
msgstr ""

#: medicare.php:4247 medicare.php:4304
msgid "7"
msgstr ""

#: medicare.php:4248 medicare.php:4305
msgid "8"
msgstr ""

#: medicare.php:4249 medicare.php:4306
msgid "9"
msgstr ""

#: medicare.php:4250 medicare.php:4307
msgid "10"
msgstr ""

#: medicare.php:4251 medicare.php:4308
msgid "15"
msgstr ""

#: medicare.php:4252 medicare.php:4309
msgid "20"
msgstr ""

#: medicare.php:4254
msgid "Tiled Format"
msgstr ""

#: medicare.php:4255
msgid "Show Title in Tiles"
msgstr ""

#: medicare.php:4265
msgid "Scroll Loading"
msgstr ""

#: medicare.php:4270
msgid "Show Sticky Posts"
msgstr ""

#: medicare.php:4290
msgid "Grid Gallery"
msgstr ""

#: medicare.php:4290
msgid "Responsive grid gallery with lightbox."
msgstr ""

#: medicare.php:4292
msgid "Images"
msgstr ""

#: medicare.php:4293
msgid "Format (e.g. 21,11,12)"
msgstr ""

#: medicare.php:4318
msgid "Links (comma-separated)"
msgstr ""

#: medicare.php:4334
msgid "Price List"
msgstr ""

#: medicare.php:4334
msgid "Price List element"
msgstr ""

#: medicare.php:4337
msgid "Subtitle"
msgstr ""

#: medicare.php:4338
msgid "Sticker Text"
msgstr ""

#: medicare.php:4339
msgid "Currency"
msgstr ""

#: medicare.php:4340
msgid "Price"
msgstr ""

#: medicare.php:4341
msgid "Items"
msgstr ""

#: medicare.php:4357
msgid "Dropdown"
msgstr ""

#: medicare.php:4357
msgid "Dropdown with links"
msgstr ""

#: medicare.php:4360
msgid "Dropdown Items"
msgstr ""

#: medicare.php:4360
msgid "name;URL separated by new line."
msgstr ""

#: medicare.php:4376
msgid "Working hours"
msgstr ""

#: medicare.php:4376
msgid "Working hours with links"
msgstr ""

#: medicare.php:4378
msgid "Working Hours"
msgstr ""

#: medicare.php:4378
msgid ""
"value;value;URL title,URL separated by new line (leave ; at the end to "
"remove link)"
msgstr ""

#: medicare.php:4410
msgid "BT Gallery"
msgstr ""

#: medicare.php:4411
msgid "Gallery widget."
msgstr ""

#: medicare.php:4430
msgid "Gallery"
msgstr ""

#: medicare.php:4434 medicare.php:4489 medicare.php:4604 medicare.php:4724
#: medicare.php:4793 medicare.php:4952 medicare.php:5145
msgid "Title:"
msgstr ""

#: medicare.php:4438
msgid "List of image IDs (comma-separated):"
msgstr ""

#: medicare.php:4463
msgid "BT Text Image"
msgstr ""

#: medicare.php:4464
msgid "Text with image."
msgstr ""

#: medicare.php:4493
msgid "Image IDs:"
msgstr ""

#: medicare.php:4497 medicare.php:4608
msgid "Text:"
msgstr ""

#: medicare.php:4523
msgid "BT Icon"
msgstr ""

#: medicare.php:4524
msgid "Icon with text and link."
msgstr ""

#: medicare.php:4589
msgid "Icon:"
msgstr ""

#: medicare.php:4612
msgid "URL or slug:"
msgstr ""

#: medicare.php:4617
msgid "Show in accent color"
msgstr ""

#: medicare.php:4620 medicare.php:4964
msgid "Target:"
msgstr ""

#: medicare.php:4636
msgid "CSS extra class:"
msgstr ""

#: medicare.php:4667
msgid "BT Recent Posts"
msgstr ""

#: medicare.php:4668
msgid "Recent posts with thumbnails."
msgstr ""

#: medicare.php:4720
msgid "Recent Posts"
msgstr ""

#: medicare.php:4728
msgid "Number of posts:"
msgstr ""

#: medicare.php:4754
msgid "BT Recent Comments"
msgstr ""

#: medicare.php:4755
msgid "Recent comments with avatars."
msgstr ""

#: medicare.php:4779
msgid "by"
msgstr ""

#: medicare.php:4789
msgid "Recent Comments"
msgstr ""

#: medicare.php:4797
msgid "Number of comments:"
msgstr ""

#: medicare.php:4827
msgid "BT Instagram"
msgstr ""

#: medicare.php:4828
msgid "Recent Instagram images."
msgstr ""

#: medicare.php:4944
msgid "Instagram"
msgstr ""

#: medicare.php:4956
msgid "Instagram username:"
msgstr ""

#: medicare.php:4960
msgid "Number of images:"
msgstr ""

#: medicare.php:4980
msgid "Hashtag:"
msgstr ""

#: medicare.php:4984 medicare.php:5157
msgid "Cache (minutes):"
msgstr ""

#: medicare.php:5084
msgid "BT Twitter"
msgstr ""

#: medicare.php:5085
msgid "Twitter feed."
msgstr ""

#: medicare.php:5149
msgid "Number of tweets:"
msgstr ""

#: medicare.php:5153
msgid "Username  (or #hashtag):"
msgstr ""

#: medicare.php:5161
msgid "Consumer key:"
msgstr ""

#: medicare.php:5165
msgid "Consumer secret:"
msgstr ""

#: medicare.php:5169
msgid "Access token:"
msgstr ""

#: medicare.php:5173
msgid "Access token secret:"
msgstr ""

#: medicare.php:5229
msgid "Portfolio Item"
msgstr ""

#: medicare.php:5238
msgid "Portfolio Categories"
msgstr ""

#: section_anims.php:7
msgid "Move to left | from right"
msgstr ""

#: section_anims.php:8
msgid "Move to right | from left"
msgstr ""

#: section_anims.php:9
msgid "Move to top | from bottom"
msgstr ""

#: section_anims.php:10
msgid "Move to bottom | from top"
msgstr ""

#: section_anims.php:11
msgid "Fade | from right"
msgstr ""

#: section_anims.php:12
msgid "Fade | from left"
msgstr ""

#: section_anims.php:13
msgid "Fade | from bottom"
msgstr ""

#: section_anims.php:14
msgid "Fade | from top"
msgstr ""

#: section_anims.php:15
msgid "Fade left | Fade right"
msgstr ""

#: section_anims.php:16
msgid "Fade right | Fade left"
msgstr ""

#: section_anims.php:17
msgid "Fade top | Fade bottom"
msgstr ""

#: section_anims.php:18
msgid "Fade bottom | Fade top"
msgstr ""

#: section_anims.php:19
msgid "Different easing | from right"
msgstr ""

#: section_anims.php:20
msgid "Different easing | from left"
msgstr ""

#: section_anims.php:21
msgid "Different easing | from bottom"
msgstr ""

#: section_anims.php:22
msgid "Different easing | from top"
msgstr ""

#: section_anims.php:23
msgid "Scale down | from right"
msgstr ""

#: section_anims.php:24
msgid "Scale down | from left"
msgstr ""

#: section_anims.php:25
msgid "Scale down | from bottom"
msgstr ""

#: section_anims.php:26
msgid "Scale down | from top"
msgstr ""

#: section_anims.php:27
msgid "Scale down | scale down"
msgstr ""

#: section_anims.php:28
msgid "Scale up | scale up"
msgstr ""

#: section_anims.php:29
msgid "Move to left | scale up"
msgstr ""

#: section_anims.php:30
msgid "Move to right | scale up"
msgstr ""

#: section_anims.php:31
msgid "Move to top | scale up"
msgstr ""

#: section_anims.php:32
msgid "Move to bottom | scale up"
msgstr ""

#: section_anims.php:33
msgid "Scale down | scale up"
msgstr ""

#: section_anims.php:34
msgid "Glue left | from right"
msgstr ""

#: section_anims.php:35
msgid "Glue right | from left"
msgstr ""

#: section_anims.php:36
msgid "Glue bottom | from top"
msgstr ""

#: section_anims.php:37
msgid "Glue top | from bottom"
msgstr ""

#: section_anims.php:38
msgid "Flip right"
msgstr ""

#: section_anims.php:39
msgid "Flip left"
msgstr ""

#: section_anims.php:40
msgid "Flip top"
msgstr ""

#: section_anims.php:41
msgid "Flip bottom"
msgstr ""

#: section_anims.php:42
msgid "Push left | from right"
msgstr ""

#: section_anims.php:43
msgid "Push right | from left"
msgstr ""

#: section_anims.php:44
msgid "Push top | from bottom"
msgstr ""

#: section_anims.php:45
msgid "Push bottom | from top"
msgstr ""

#: section_anims.php:46
msgid "Push left | pull right"
msgstr ""

#: section_anims.php:47
msgid "Push right | pull left"
msgstr ""

#: section_anims.php:48
msgid "Push top | pull bottom"
msgstr ""

#: section_anims.php:49
msgid "Push bottom | pull top"
msgstr ""

#: section_anims.php:50
msgid "Fold left | from right"
msgstr ""

#: section_anims.php:51
msgid "Fold right | from left"
msgstr ""

#: section_anims.php:52
msgid "Fold top | from bottom"
msgstr ""

#: section_anims.php:53
msgid "Fold bottom | from top"
msgstr ""

#: section_anims.php:54
msgid "Move to right | unfold left"
msgstr ""

#: section_anims.php:55
msgid "Move to left | unfold right"
msgstr ""

#: section_anims.php:56
msgid "Move to bottom | unfold top"
msgstr ""

#: section_anims.php:57
msgid "Move to top | unfold bottom"
msgstr ""

#: section_anims.php:58
msgid "Room to left"
msgstr ""

#: section_anims.php:59
msgid "Room to right"
msgstr ""

#: section_anims.php:60
msgid "Room to top"
msgstr ""

#: section_anims.php:61
msgid "Room to bottom"
msgstr ""

#: section_anims.php:62
msgid "Cube to left"
msgstr ""

#: section_anims.php:63
msgid "Cube to right"
msgstr ""

#: section_anims.php:64
msgid "Cube to top"
msgstr ""

#: section_anims.php:65
msgid "Cube to bottom"
msgstr ""

#: section_anims.php:66
msgid "Carousel to left"
msgstr ""

#: section_anims.php:67
msgid "Carousel to right"
msgstr ""

#: section_anims.php:68
msgid "Carousel to top"
msgstr ""

#: section_anims.php:69
msgid "Carousel to bottom"
msgstr ""

#: section_anims.php:70
msgid "Sides"
msgstr ""

#: section_anims.php:71
msgid "Slide"
msgstr ""
