��          |      �          &   !     H     X  D   `     �  )   �  9   �          %     =  2   X  /  �  +   �     �     �  u        }  >   �  N   �     (     8     P  2   k                            	             
                  &#8220;%s&#8221; has failed to upload. Chunk Size (kb) Dismiss Enables large file uploads in the built-in WordPress media uploader. Max Retries Maximum Upload Size (MB) (0 for no limit) The file size has exceeded the maximum file size setting. Trevor Anderson Tuxedo Big File Uploads https://github.com/andtrev https://github.com/andtrev/Tuxedo-Big-File-Uploads PO-Revision-Date: 2021-03-17 00:38:42+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Generator: GlotPress/3.0.0-alpha.2
Language: fr
Project-Id-Version: Plugins - Tuxedo Big File Uploads - Stable (latest release)
 « %s » n’a pas pu être téléversé. Taille du morceau ( ko) Ignorer Permet le téléversement de fichiers volumineux dans l’outil de téléversement de médias intégré de WordPress. Nombre maximal de tentatives Taille maximale de téléversement (Mo) (0 pour aucune limite) La taille du fichier a dépassé le réglage de la taille maximale de fichier. Trevor Anderson Tuxedo Big File Uploads https://github.com/andtrev https://github.com/andtrev/Tuxedo-Big-File-Uploads 